//	***************************************************************************************************************
// 	*** imagebg.js *** - javascript code for displaying scaled images on full size canvas						***
//                      imgIndex - parameter for number of image to dsiplay as per imagefilename+suffix.jpg		***
// 	*** Lech Jankowski 13/12/2013 																				***
//	***************************************************************************************************************
	
// creating array for background images collection

var imgArray = new Array(6); // creating an array collection of background images of 6 in total

// loading upfront all images for backgrounds and storing into an array

for (i=0;i<imgArray.length;i++)
{	
	//imgArray[i] = new Image();
	imgArray[i] = document.createElement("img"); // this will work in Chrome too
	imgArray[i].src = "images/bg"+(i+1)+".jpg";
}

function loadImageBg(imgIndex){

	//canvas init

	var canvas = document.getElementById("canvasImageBg");
	var context = canvas.getContext('2d');

	//canvas & backround image dimensions
	
	var W2 = window.innerWidth;
	var H2 = window.innerHeight;
	var iw = imgArray[imgIndex-1].width;
	var ih = imgArray[imgIndex-1].height;
	
	//canvas setting size

	canvas.width = W2;
	canvas.height = H2;

	// setting canvas and drawing image on in size to fit window scale
	
	//context.fillRect(0,0,W2,H2); - redundant
	context.drawImage(imgArray[imgIndex-1],0,0,iw,ih,0,0,W2,H2);

/*  // this WIP code meant to display drawing in a fading out animation mode, however is corrupting on activating for many times /few images from memory interfered with each other

	if (imgArray[imgIndex].complete)
	{
		imgArray[imgIndex-1].addEventListener('load', function() {
		context.globalAlpha = 0;
		var imgFadeInter = setInterval(function(){
			// clearContext();  a function that clears the canvas ?-don't use now(not necessary)
			context.fillRect(0,0,W2,H2);
			context.globalAlpha += 0.01;
			context.drawImage(imgArray[imgIndex-1],0,0,iw,ih,0,0,W2,H2);
			if(context.globalAlpha == 1){ 
				   clearInterval(imgFadeInter); 
			}
			}, 16); // 16ms because jQuery says so      
		});
	}
	else
	{
		context.drawImage(imgArray[imgIndex],0,0,iw,ih,0,0,W2,H2);
	}
*/
	
}
