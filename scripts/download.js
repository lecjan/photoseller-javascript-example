	window.onload = startRegistration;


	function startRegistration() {
		document.forms[0].username.focus();
		document.forms[0].onsubmit = checkRegistration;
		document.forms[0].onreset = resetRegistration;
	}
	
	function resetRegistration() {
		location.reload();
	}

	function checkRegistration() {
			    alert("before checked");

		if (document.forms[0].username.value.length == 0) {
			alert("Name is a required field");
			document.forms[0].username.focus();
			return false;
		} else if (document.forms[0].usersurname.value.length == 0){
			document.forms[0].usersurname.focus();
			alert("Surname is a required field");
			return false;
		} else if (document.forms[0].street.value.length == 0){
			document.forms[0].street.focus();
			alert("Street address is a required field");
			return false;
		} else if (document.forms[0].city_town.value.length == 0){
			document.forms[0].city_town.focus();
			alert("City/Town address is a required field");
			return false;
		} else if (document.forms[0].county.county.selectedIndex == 0){
			document.forms[0].county.focus();
			alert("County address is a required field");
			return false;
		} else {
		    alert("checked");
			storeRegistration();
			return true;			
		}
	}
	
	function storeRegistration() {
		localStorage.RegName = document.forms[0].username.value;
		localStorage.RegSurname = document.forms[0].usersurname.value;
		localStorage.RegStreet = document.forms[0].street.value;
		localStorage.RegCityTown = document.forms[0].city_town.value;
		localStorage.RegCounty = document.forms[0].county.options[document.forms[0].county.selectedIndex].value;
	}

